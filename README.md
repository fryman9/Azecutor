# Azecutor

A FreeCAD macro that allows to execute a recorded macro step by step
with a specified delay between steps.

## Authors
The project was initially written and being developed by "AZ Company
Group" LLC (https://www.gkaz.ru/) You can find the list of
contributors in `AUTHORS` file.

## Dependencies
- Guietta

## Installation

Install the dependencies
```
pip3 install guietta --no-deps
```

Note `--no-deps` option: without it, Guietta will pull all the dependencies it
requires and that in turn may break your FreeCAD installation.  If you faced
with this problem, just remove Guietta and all the dependencies it installed,
and install it as shown above.

Install Azecutor:
```
cd ~/.FreeCAD/Macro/
git clone https://gitlab.com/gkaz/Azecutor.git
ln -s Azecutor/azecutor.FCMacro .
```
