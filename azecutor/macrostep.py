import random
import string
from textwrap import dedent
from typing import List
from pathlib import Path

def random_id() -> str:
    return "".join(random.choices(string.ascii_uppercase + string.digits, k=5))


class MacroStep:
    def __init__(
        self,
        code_lines: List[str],
        workbench=None,
        file_line_offset: int = 0,
        src_path: Path = None
    ):
        self.id = random_id()
        self.code = self._prepare(dedent("\n".join(
            l.rstrip("\r\n") for l in code_lines
            if not l.isspace()
        )))
        self.file_line_offset = file_line_offset
        self.file_path = src_path
        self.workbench = workbench

    def _prepare(self, code):
        return '\n'.join([
           f"FreeCAD.ActiveDocument.openTransaction('{self.id}')",
           dedent(code),
           "FreeCAD.ActiveDocument.commitTransaction()",
           "App.ActiveDocument.recompute()",
           "Gui.SendMsgToActiveView('ViewFit')"
        ])

    def __str__(self):
        return self.code

    def __repr__(self):
        return self.code

    def get_workbench(self):
        return self.workbench
