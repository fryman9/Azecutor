import logging as log
from re import match
from typing import Union, List, Dict
from pathlib import Path
from Azecutor.azecutor.macrostep import MacroStep

Step = Union[str, MacroStep]

def coding_line_p(line: str):
    '''Check if a LINE is a line that specifies the text encoding coding'''
    return (match(r"#.*coding[=:]\s*([-\w.]+).*", line) != None)


def import_p(line: str):
    '''Check if a LINE contains a Python import directive.'''
    return (match(r"#?import\s+.*$", line) != None)


def gui_command_p(line: str):
    '''Check if a LINE is a GUI command'''
    return (line.find("gui.runcommand") > -1)


def gui_selection_p(line: str):
    '''Check if a LINE contains a GUI selection command.'''
    return (match(r"#.*Gui.Selection.*", line) != None)


def transaction_begin_p(line: str):
    '''Check if a LINE starts a transaction.'''
    return (match("### Begin .*", line) != None)


def transaction_end_p(line: str):
    '''Check if a LINE ends a transaction.'''
    return (match("### End .*", line) != None)


def remove_prefix(line: str, prefix: str):
    '''Remove a PREFIX from a LINE.  Return a new string.'''
    if line.startswith(prefix):
        return line[len(prefix):]
    return line


def get_transaction_workbench(line: str):
    '''Get the name of a workbench from a transaction line.'''
    m = match(r"### Begin command ([A-z]+)_.*", line)
    return m.group(1)


def parse_file(in_lines: List[str],
               src_path: Path,
               ignore_gui_commands=False,
               show_selection=False) -> (List[str], List[Step]):
    '''Parse input IN_LINES and produce two lists as the result.'''
    out_lines: List[Step] = []
    imports: List[str]    = []
    step_span_start       = -1
    current_workbench     = None

    def prepare_lines(lines, uncomment=False):
        return [
            remove_prefix(remove_prefix(l, "# "), "#")
            if uncomment else l
            for l in lines
            if not l.isspace()
        ]

    def push_macro(lines, workbench=None, uncomment=True):
        lines = prepare_lines(lines, uncomment)
        if len(lines) > 0:
            out_lines.append(MacroStep(lines, workbench, step_span_start, src_path))

    unspanned_lines: List[str] = []

    for i, line in enumerate(in_lines):
        line_norm = line.strip().lower()
        is_line_commented = line.startswith("#")
        if coding_line_p(line_norm):
            continue
        elif gui_command_p(line_norm) and ignore_gui_commands:
            continue
        elif import_p(line_norm):
            line = line.lstrip("# ")
            imports.append(line)
        elif transaction_begin_p(line):
            log.debug("$$ got begin")
            if len(unspanned_lines) > 0:
                push_macro(unspanned_lines, workbench=current_workbench)
                unspanned_lines.clear()
            current_workbench = get_transaction_workbench(line)
            log.debug(f"Current workbench: {current_workbench}")
            step_span_start = i + 1
        elif transaction_end_p(line) or line.lstrip("#").isspace():
            if len(unspanned_lines) > 0:
                push_macro(unspanned_lines, workbench=current_workbench)
                unspanned_lines.clear()
        elif gui_selection_p(line) \
             and show_selection:
            unspanned_lines.append(line.lstrip("# "))
        elif not is_line_commented:
            unspanned_lines.append(line.rstrip("\r\n"))

    return imports, out_lines
