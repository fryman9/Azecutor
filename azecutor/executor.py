import logging as log
import sys
import traceback
from textwrap import indent, dedent
from typing import Union, List, Dict

import FreeCADGui as Gui
import FreeCAD as App
from Azecutor.azecutor.macrostep import MacroStep

Step = Union[str, MacroStep]

class InterpreterError(Exception):
    pass


def wrap_import_safe(line):
    line = line.strip()
    wrapped = dedent(f"""\
        try:
            {line}
        except:
            print(r\"""{line} failed, skipping.\""")
        """).strip()
    log.debug(indent(wrapped, "import[----]> "))
    return wrapped


def remove_redundants():
    '''Remove redundant constraints from the current sketch.'''
    sketches = App.ActiveDocument.findObjects("Sketcher::SketchObject")
    number = 1
    total  = len(sketches)
    if total > 0:
        log.info(f"Checking {total} sketches for redundant constraints ...")
        for sketch in sketches:
            if sketch.isValid():
                log.info(f"  {number}/{total}: OK")
            else:
                sketch.autoRemoveRedundants()
                App.ActiveDocument.recompute()
                log.info(f"  {number}/{total}: Redundants removed")
            number += 1
        log.info(f"Checking sketches for redundant constraints ... done")


def execute_step(step:    Step,
                 context: Dict[str, object],
                 auto_remove_redundants = True) -> Dict[str, object]:
    log.debug("Context: " + ", ".join(k for k in context.keys() if "__" not in k))
    if isinstance(step, MacroStep):
        log.debug("Workbench: " + (step.get_workbench() if step.get_workbench() != None
                                   else "(None)"))
    log.debug("Commands:\n" + indent(
        str(step),
        f"macro[{step.id}]> " if isinstance(step, MacroStep)
        else "line [-----]> "
    ))
    try:
        exec(str(step), context)
        if auto_remove_redundants:
            remove_redundants()
    except SyntaxError as e:
        error_class = e.__class__.__name__
        details = e.args[0]
        line_number = e.lineno
    except Exception as e:
        error_class = e.__class__.__name__
        details = e.args[0]
        cl, exc, tb = sys.exc_info()
        line_number = traceback.extract_tb(tb)[-1][1]
    else:
        return context

    raise InterpreterError(
        f"{error_class} at line {line_number:d} of {step}: {details}"
    )
